"""
Script intended to be executed as part of an AWS Lambda function.

It is supposed to be triggerd by AWS S3 when a new JSON file is put into a
specific bucket, and the content of such JSON file is the configuration this
script will work with in order to take actions related to the AWS EC2 instance
which uploaded the file to the bucket.

JSON file should have this structure:

{
    "instance_name": <string>,
    "messages": [{'message': <string>, 'message_type': <string>}],
    "logs2cloudwatch": <true|false>,
    "mandatory_apps_result": {
        "action": <true|false>,
        "messages": {
            <app_name>: <string>
        }
    },
    "not_mandatory_apps_result": {
        "action": <true|false>,
        "messages": {
            <app_name>: <string>
    },
    "action": <info|stop|terminate|detach>
}

"instance_name": name of the AWS EC2 instance (Private DNS property of the
    instance) this script will take actions against.
"messages": messages of general purposes, commonly used to specify general
    errors and warnings. This script will send them to CloudWatch if
    'logs2cloudwatch' is True.
"logs2cloudwatch": whether this script should or not send the messages to
    'Cloud Watch' service.
"mandatory_apps_result":
    "action": if True, it means it was necessary to take actions with any
    at least one of the mandatory applications; in this case, messages printed
    in CloudWatch will be logged as 'warnings'. Otherwise, the will be logged
    as 'info'.
    "messages": specific messages for mandatory applications.
"not_mandatory_apps_result": (same as 'mandatory_apps_result', but for
    not mandatory applications).
"action": action this script will try to execute against the instance with the
    name defined in 'instance_name'.
"""


import boto3
import json
import logging
import sys
import traceback


def _get_instance_id(ec2_client, instance_name):
    """Fetch and return the instance ID from instance_name."""
    filters = [{
        'Name': 'private-dns-name',
        'Values': [instance_name]
    }]
    response = ec2_client.describe_instances(Filters=filters)["Reservations"]
    return response[0]['Instances'][0]['InstanceId']


def _detach(instance_name, logger):
    """Detach the instance with instance_name from its autoscaling group."""
    logger.info("Trying to detach instance {0}".format(instance_name))
    ec2_client = boto3.client('ec2')
    instance_id = _get_instance_id(ec2_client, instance_name)
    if not instance_id:
        raise Exception("No ID could be retrieved from instance {0}".format(
            instance_name
        ))
    autoscaling_client = boto3.client('autoscaling')
    # autoscaling group instance_name belongs to
    response = autoscaling_client.describe_auto_scaling_instances(
        InstanceIds=[instance_id],
        MaxRecords=1
    )
    if response:
        asg = response['AutoScalingInstances'][0]['AutoScalingGroupName']
        autoscaling_client.detach_instances(
            InstanceIds=[instance_id],
            AutoScalingGroupName=asg,
            ShouldDecrementDesiredCapacity=False
        )
        logger.info(("Call was made to the instance {0} in order to detach " +
                    "it from autoscaling group {1}.").
                    format(instance_name, asg))
    else:
        logger.info("No autoscaling group was found for instance {0}".format(
            instance_name
        ))


def _stop(instance_name, logger):
    """Stop instance with instance_name."""
    logger.info("Trying to stop instance with name {0}".format(instance_name))
    client = boto3.client('ec2')
    instance_id = _get_instance_id(client, instance_name)
    if not instance_id:
        raise Exception("No ID could be retrieved from instance {0}".format(
            instance_name
        ))
    client.stop_instances(InstanceIds=[instance_id])
    logger.info("Call was made to the instance {0} in order to stop it.".
                format(instance_name))


def _terminate(instance_name, logger):
    """Terminate instance with instance_name."""
    logger.info("Trying to terminate instance with name {0}".format(
        instance_name
    ))
    client = boto3.client('ec2')
    instance_id = _get_instance_id(client, instance_name)
    if not instance_id:
        raise Exception("No ID could be retrieved from instance {0}".format(
            instance_name
        ))
    client.terminate_instances(InstanceIds=[instance_id])
    logger.info("Call was made to the instance {0} in order to terminate it.".
                format(instance_name))


def _get_file_content(s3_client, bucket_name, file_name):
    """Read and return the content of the JSON file in the bucket.

    Keyword arguments:

    s3_client -- client object to connect to AWS S3 service.
    bucket_name -- name of the bucket containing the file.
    file_name -- name of the file in the bucket.
    """
    file_obj = s3_client.get_object(Bucket=bucket_name, Key=file_name)
    return file_obj['Body'].read().decode('utf-8')


def _send_logs2cloudwatch(conf, logger):
    """Print logs coming in conf: general logs, logs for mandatory apps and
    logs for non-mandatory apps.
    """
    def _log_apps_messages(app_conf):
        action = app_conf['action']
        logger_method = 'warning' if action else 'info'
        messages = app_conf['messages']
        for app in messages:
            message = messages[app]
            final_message = " - ".join(["Mandatory app", app, message])
            getattr(logger, logger_method)(final_message)
    # send general messages to log
    for message in conf['messages']:
        try:
            getattr(logger, message['message_type'])(message['message'])
        except AttributeError as e:
            logger.error(str(e))
    # mandatory apps messages
    _log_apps_messages(conf['mandatory_apps_result'])
    # non-mandatory apps messages
    _log_apps_messages(conf['not_mandatory_apps_result'])


def _apply_action(bucket_file_content, logger):
    """Execute the action specified in the file fetched from AWS bucket.

    Keyword arguments:

    bucket_file_content -- dictionary containing the action to be executed by
        this lambda function, and the messages to be sent to CloudWatch (if
        defined).
    logger -- object to send logs to CloudWatch (if defined).
    """
    conf = json.loads(bucket_file_content)
    instance_name = conf['instance_name']
    if instance_name == "no-server-name":
        return "Name of the server couldn't be fetched. No action taken."
    if 'logs2cloudwatch' in conf and conf['logs2cloudwatch']:
        _send_logs2cloudwatch(conf, logger)
    actions = {
        "detach": _detach,
        "stop": _stop,
        "terminate": _terminate
    }
    action = conf['action']
    # only actions different from info need to be executed since all the
    # information was already sent to the logs (CloudWatch)
    if action != 'info':
        actions.get(action,
                    lambda a, b:
                    logger.critical("Action '{0}' not allowed.".
                                    format(action)))(instance_name, logger)


def _delete_file(s3_client, bucket_name, file_name):
    """Delete file_name from bucket_name using s3_client to connect to S3."""
    if bucket_name and file_name:
        try:
            s3_client.delete_object(Bucket=bucket_name, Key=file_name)
        except Exception:
            pass


def lambda_handler(event, context):
    """Read file from an AWS S3 bucket and execute the specified action. Send
    logs to CloudWatch (if so is defined as part of the content of the file).
    Delete file once its content has been read.

    This function is the entry point to this script.
    """
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    s3 = None
    bucket_name = ""
    file_name = ""
    try:
        if not event:
            raise Exception("No event was provided.")
        s3 = boto3.client("s3")
        s3_object = event['Records'][0]
        bucket_name = str(s3_object['s3']['bucket']['name'])
        file_name = str(s3_object['s3']['object']['key'])
        bucket_file_content = _get_file_content(s3, bucket_name, file_name)
        result = _apply_action(bucket_file_content, logger)
        if result:
            logger.warning(result)
    except Exception:
        exception_type, exception_value, exception_traceback = sys.exc_info()
        traceback_string = traceback.format_exception(exception_type,
                                                      exception_value,
                                                      exception_traceback)
        err_msg = json.dumps({
            "error_type": exception_type.__name__,
            "error_message": str(exception_value),
            "stack_trace": traceback_string
        })
        logger.error(err_msg)
    finally:
        if s3:
            _delete_file(s3, bucket_name, file_name)
        else:
            logger.warning("Health Checker JSON file couldn't be removed.")
